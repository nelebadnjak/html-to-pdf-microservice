# HTML to PDF microservice



## Easy way to convert HTML to PDF packed into Docker container

This is python application which uses Flask and pdfkit library to convert HTML to PDF files using HTTP request.
[Pdfkit](https://pypi.org/project/pdfkit/) is based on [wkhtmltopdf](https://wkhtmltopdf.org/)


## Installation
1. Install [Docker](https://get.docker.com/)
2. docker pull .......
3. docker run -d .........

## Usage
- curl example
- documentation

## License
[WTFPL](https://en.wikipedia.org/wiki/WTFPL)


